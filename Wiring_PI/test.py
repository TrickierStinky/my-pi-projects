#!/usr/bin/python
# -*- coding: utf-8 -*-
# using wiringPi GPIO method
# also using PWM on gpio pin 12
# note that the contrast may need adjustment between displays
# also note that I took out wiringPi's built in shiftOut routine as it caused corruption, possibly
# because it is too fast for the display
# Texy 18/8/12

import wiringpi
import time

#gpio's :
SCLK = 24 # gpio pin 18 = wiringpi no. 5
DIN =  23 # gpio pin 16 = wiringpi no. 4
DC =  22 # gpio pin 15 = wiringpi no. 3
RST =  17 # gpio pin 11 = wiringpi no. 0
LED = 18 # gpio pin 12 = wiringpi no. 1

font =[
0x7E, 0x11, 0x11, 0x11, 0x7E, # A
0x7F, 0x49, 0x49, 0x49, 0x36, # B
0x3E, 0x41, 0x41, 0x41, 0x22, # C
0x7F, 0x41, 0x41, 0x22, 0x1C, # D
0x7F, 0x49, 0x49, 0x49, 0x41, # E
0x7F, 0x09, 0x09, 0x09, 0x01, # F
0x3E, 0x41, 0x49, 0x49, 0x7A, # G
0x7F, 0x08, 0x08, 0x08, 0x7F, # H
0x00, 0x41, 0x7F, 0x41, 0x00, # I
0x20, 0x40, 0x41, 0x3F, 0x01, # J
0x7F, 0x08, 0x14, 0x22, 0x41, # K
0x7F, 0x40, 0x40, 0x40, 0x40, # L
0x7F, 0x02, 0x0C, 0x02, 0x7F, # M
0x7F, 0x04, 0x08, 0x10, 0x7F, # N
0x3E, 0x41, 0x41, 0x41, 0x3E, # O
0x7F, 0x09, 0x09, 0x09, 0x06, # P
0x3E, 0x41, 0x51, 0x21, 0x5E, # Q
0x7F, 0x09, 0x19, 0x29, 0x46, # R
0x46, 0x49, 0x49, 0x49, 0x31, # S
0x01, 0x01, 0x7F, 0x01, 0x01, # T
0x3F, 0x40, 0x40, 0x40, 0x3F, # U
0x1F, 0x20, 0x40, 0x20, 0x1F, # V
0x3F, 0x40, 0x38, 0x40, 0x3F, # W
0x63, 0x14, 0x08, 0x14, 0x63, # X
0x07, 0x08, 0x70, 0x08, 0x07, # Y
0x61, 0x51, 0x49, 0x45, 0x43, # Z
]

def main():
  start = time.time()
  begin(0xb3) # contrast - may need tweaking for each display
  gotoxy(28,0)
  text("HI")
  gotoxy(18,2)
  text("THERE")
  gotoxy(26,4)
  text("PEEPS")
  finish = time.time()
  print ("Total time : ",finish - start)
  print

def gotoxy(x,y):
  lcd_cmd(x+128)
  lcd_cmd(y+64)

def text(words):
  for i in range(len(words)):
    display_char(words[i])

def display_char(char):
  index=(ord(char)-65)*5
  if ord(char) >=65 and ord(char) <=90:
    for i in range(5):
      lcd_data(font[index+i])
    lcd_data(0) # space inbetween characters
  elif ord(char)==32:
      lcd_data(0)
      lcd_data(0)
      lcd_data(0)
      lcd_data(0)
      lcd_data(0)
      lcd_data(0)

def cls():
  gotoxy(0,0)
  for i in range(84):
    for j in range(6):
      lcd_data(0)

def setup():
  # set pin directions
  wiringpi.wiringPiSetupGpio()
  wiringpi.pinMode(DIN, 1)
  wiringpi.pinMode(SCLK, 1)
  wiringpi.pinMode(DC, 1)
  wiringpi.pinMode(RST, 1)
  wiringpi.pinMode(LED, 2) # LED set up as PWM
  print ("setup ok")
  wiringpi.pwmWrite(LED, 255)

def begin(contrast):
  setup()
  # toggle RST low to reset
  wiringpi.digitalWrite(RST, 0)
  wiringpi.digitalWrite(LED, 0)
  time.sleep(0.100)
  wiringpi.digitalWrite(RST, 1)
  lcd_cmd(0x21) # extended mode
  lcd_cmd(0x14) # bias
  lcd_cmd(contrast) # vop
  lcd_cmd(0x20) # basic mode
  lcd_cmd(0xc) # non-inverted display
  cls()

def SPI(c):
  # data = DIN
  # clock = SCLK
  # MSB first
  # value = c
  for i in xrange(8):
    wiringpi.digitalWrite(DIN,((c & (1 << (7-i))) > 0))
    wiringpi.digitalWrite(SCLK, 1)
    wiringpi.digitalWrite(SCLK, 0)
#  wiringpi.shiftOut(DIN, SCLK, 1, c)

def lcd_cmd(c):
  wiringpi.digitalWrite(DC, 0)
  SPI(c)

def lcd_data(c):
  wiringpi.digitalWrite(DC, 1)
  SPI(c)

if __name__ == "__main__":
  main()
